import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_provider/source/providers/courses_info.dart';
import 'package:test_provider/source/providers/users_info.dart';

class SuperText extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final usersInfo = Provider.of<UsersInfo>(context);
    final coursesInfo = Provider.of<CoursesInfo>(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          usersInfo.user,
          style: TextStyle(
            fontSize: 30.0,
            color: usersInfo.colorBase
          ),
        ),
        Text(
          coursesInfo.course,
          style: TextStyle(
            fontSize: 25.0,
          ),
        ),
      ],
    );
  }
}
