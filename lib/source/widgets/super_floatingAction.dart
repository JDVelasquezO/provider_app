import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_provider/source/providers/courses_info.dart';
import 'package:test_provider/source/providers/users_info.dart';

class SuperFloatingAction extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final usersInfo = Provider.of<UsersInfo>(context);
    final coursesInfo = Provider.of<CoursesInfo>(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        FloatingActionButton(
          child: Icon(Icons.accessibility),
          backgroundColor: Colors.red,
          onPressed: () {
            usersInfo.user = 'Daniel';
            coursesInfo.course = 'Matematica Intermedia 1';
          },
        ),
        SizedBox(height: 10.0),
        FloatingActionButton(
          child: Icon(Icons.ac_unit),
          backgroundColor: Colors.blue,
          onPressed: () {
            usersInfo.user = 'Gaby';
            coursesInfo.course = 'Física 2';
          },
        ),
      ],
    );
  }
}