import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_provider/source/providers/users_info.dart';
import 'package:test_provider/source/widgets/super_floatingAction.dart';
import 'package:test_provider/source/widgets/super_text.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final usersInfo = Provider.of<UsersInfo>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(usersInfo.user),
      ),
      body: Center(child: SuperText()),
      floatingActionButton: SuperFloatingAction(),
    );
  }
}