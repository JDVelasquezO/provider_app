import 'package:flutter/material.dart';

class UsersInfo with ChangeNotifier {
  
  String _user = 'Daniel';
  Color colorBase = Colors.blue;

  get user {
    return _user;
  }

  set user ( String name ) {
    this._user = name;
    this.colorBase = ( name == 'Gaby' ) ? Colors.blue : Colors.red;
    notifyListeners();
  }
}