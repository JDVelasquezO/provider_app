import 'package:flutter/material.dart';

class CoursesInfo with ChangeNotifier {
  
  String _course = 'Toca un boton';

  get course {
    return _course;
  }

  set course ( String name ) {
    this._course = name;
    notifyListeners();
  }
}
