import 'package:flutter/material.dart';
import 'package:test_provider/source/pages/HomePage.dart';
import 'package:provider/provider.dart';
import 'package:test_provider/source/providers/courses_info.dart';
import 'package:test_provider/source/providers/users_info.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  /*@override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        builder: (context) => UsersInfo(),
        child: MaterialApp(
        title: 'Material App',
        initialRoute: 'home',
        debugShowCheckedModeBanner: false,
        routes: {
          'home': (context) => HomePage(),
        },
      ),
    );
  }*/

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider( builder: (context) => UsersInfo() ),
          ChangeNotifierProvider( builder: (context) => CoursesInfo() ),
        ],
        child: MaterialApp(
        title: 'Material App',
        initialRoute: 'home',
        debugShowCheckedModeBanner: false,
        routes: {
          'home': (context) => HomePage(),
        },
      ),
    );
  }
}